class Point:

    def __init__(self, lat: float, long: float):
        self.long = long
        self.lat = lat

    def __str__(self):
        return f'({self.lat}, {self.long})'
    
    def __repr__(self):
        return f'({self.lat}, {self.long})'
    
    def __eq__(self, other):
        return self.lat == other.lat and self.long == other.long
    
    def __hash__(self):
        return hash((self.lat, self.long))