from app.model.database import Database

class GraphBuilder:

    def __init__(self) -> None:
        self.database_instance = Database()

    def create_nodes_and_edges(self):
        # get all nodes
        poi = list(self.database_instance.getData("graph_nodes", {"node_type": {"$in": ["EVENT_PLACE", "POI"]}}))
        bikes = list(self.database_instance.getData("graph_nodes", {"node_type": "BIKE_PARKING"}))
        cars = list(self.database_instance.getData("graph_nodes", {"node_type": "AUTO_PARKING"}))
        metro = list(self.database_instance.getData("graph_nodes", {"node_type": "METRO"}))
        nodes = []
        edges = []

        # CREATE NODES and EDGES
        # connect all point of interests
        for node in poi:
            for node2 in poi:
                if node['index'] != node2['index']:
                    edges.append((node['index'], node2['index']))
            nodes.append({"index": node['index'], "node_type": node['node_type'], "name": node['name']})

        # parcheggi bici -> punti di interesse
        for bike_park in bikes:
            for node in poi:
                edges.append((bike_park['index'], node['index']))
            nodes.append({"index": bike_park['index'], "node_type": bike_park['node_type'], "name": node['name']})

        # parcheggi auto -> punti di interesse
        for car_park in cars:
            for node in poi:
                edges.append((car_park['index'], node['index']))
            nodes.append({"index": car_park['index'], "node_type": car_park['node_type'], "name": car_park['name']})

        # fermate metro -> punti di interesse
        for metro_stop in metro:
            for node in poi:
                edges.append((metro_stop['index'], node['index']))
            nodes.append({"index": metro_stop['index'], "node_type": metro_stop['node_type'], "name": metro_stop['name']})

        return nodes, edges