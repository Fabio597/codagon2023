import os
from pymongo import MongoClient

MONGO_INITDB_ROOT_USERNAME = os.environ['MONGO_INITDB_ROOT_USERNAME']
MONGO_INITDB_ROOT_PASSWORD = os.environ['MONGO_INITDB_ROOT_PASSWORD']
MONGO_ENDPOINT= os.environ['MONGO_ENDPOINT']
MONGO_PORT = os.environ['MONGO_PORT']

class Database:

    def __init__(self) -> None:
        self.mongo_client = MongoClient(
            MONGO_ENDPOINT, 
            int(MONGO_PORT), 
            username=MONGO_INITDB_ROOT_USERNAME, 
            password=MONGO_INITDB_ROOT_PASSWORD, 
            authSource='admin'
        )
        
    def get_collection(self, collection_name):
        return self.mongo_client["codagon"][collection_name]
    
    def insertManyData(self, collection_name, data):
        collection = self.get_collection(collection_name)
        collection.insert_many(data)

    def insertOneData(self, collection_name, data):
        collection = self.get_collection(collection_name)
        collection.insert_one(data)

    def getData(self, collection_name, query):
        collection = self.get_collection(collection_name)
        return collection.find(query, {"_id": 0})
    
    def getOneData(self, collection_name, query):
        collection = self.get_collection(collection_name)
        return collection.find_one(query, {"_id": 0})
    
    def create_index(self, collection_name, column_name, unique=True):
        collection = self.get_collection(collection_name)
        collection.create_index([ (column_name, 1) ], unique=unique)