import networkx as nx
from app.model.database import Database

class GraphWrapper:

    def __init__(self, nodes, edges) -> None:
        self.database_instance = Database()
        self.graph = nx.Graph()
        for node in nodes:
            self.graph.add_node(node['index'], node_type=node['node_type'], name=node['name'])
        self.graph.add_edges_from(edges)

    def delete_all_edges(self):
        self.graph.remove_edges_from(list(self.graph.edges()))

    def add_edges(self, edges):
        self.graph.add_edges_from(edges)

    def get_different_paths(self, starting_node: int, ending_node: int, max_paths=3):
        paths = []
        for path in nx.all_simple_paths(self.graph, source=starting_node, target=ending_node, cutoff=max_paths):
            paths.append(path)
        return paths
    
    def filter_paths(self, starting_node: int, ending_node: int, types: list[str], top_k = 5):
        filtered_paths = []
        poi_places = list(self.database_instance.getData("graph_nodes", {"node_type": {"$in": ["POI"]}, "type": {"$in": types}}))
        poi_index_places = set([poi_place['index'] for poi_place in poi_places])
        all_paths = self.get_different_paths(starting_node, ending_node, max_paths=3)
        for path in all_paths:
            if len(path) > 2:
                middle_points = path[1:-1]
                score = 0
                for node in middle_points:
                    if node in poi_index_places:
                        score += 1
                if score != 0:
                    filtered_paths.append({"path": path, "score": score / len(middle_points)})
        
        filtered_paths = sorted(filtered_paths, key=lambda d: d['score'], reverse=True) [0:top_k]

        return filtered_paths

    def filter_journeys(self, journeys):
        new_journeys = []
        for journey in journeys:
            sections = []
            for section in journey['sections']:
                mode = ""
                if "mode" in section:
                    mode = section['mode']
                current_section = {
                    "duration": section['duration'],
                    "type": section['type'],
                    "co2_emission": section['co2_emission']["value"],
                    "mode": mode,
                }
                sections.append(current_section)
            current_journey = {
                "duration": journey['duration'],
                "type": journey['type'],
                "co2_emission": journey['co2_emission'],
                "tags": journey['tags'],
                "sections": sections
            }
            new_journeys.append(current_journey)
        return new_journeys
    
    # preferences: ["walking", "bike", "public_transport", "carnopark"]
    def score_journeys(self, journeys, preferences = []):
        new_journeys = []
        for journey in journeys:
            sections = []
            score = 0
            for section in journey['sections']:
                mode = ""
                if "mode" in section:
                    mode = section['mode']
                    if mode in preferences:
                        score += 1
                else:
                    if section["type"] in preferences:
                        score += 1
                additional_information, display_information = "", ""
                if "additional_informations" in section:
                    additional_information = section["additional_informations"]
                if "display_informations" in section:
                    display_information = section["display_informations"]
                current_section = {
                    "duration": section['duration'],
                    "type": section['type'],
                    "co2_emission": section['co2_emission']["value"],
                    "mode": mode,
                    "additional_informations": additional_information,
                    "display_informations": display_information
                }
                sections.append(current_section)

            current_journey = {
                "duration": journey['duration'],
                "type": journey['type'],
                "co2_emission": journey['co2_emission'],
                "tags": journey['tags'],
                "sections": sections,
                "distances": journey['distances'],
                "score": score / len(journey['sections'])
            }
            score = 0
            new_journeys.append(current_journey)
        return new_journeys
    
    def create_routes(self, starting_node, ending_node, types, preferences, top_k):
        extracted_paths =self.filter_paths(starting_node, ending_node, types, top_k)
        routes = []
        for single_path in extracted_paths:
            path = single_path['path']
            node_index = 0
            path_object = {
                "path_length": len(path),
                "journeys": []
            }
            while node_index < len(path):
                if node_index != len(path) - 1:
                    node_data = self.database_instance.getOneData("graph_nodes", {"index": path[node_index]})
                    connection = self.database_instance.getOneData("navitia_routes", {"start_node": path[node_index], "end_node": path[node_index + 1]})
                    path_object["journeys"].append({"node": node_data})
                    if connection is not None and "journeys" in connection["navitia_response"]:
                        journeys = self.score_journeys(connection["navitia_response"]["journeys"], preferences)
                        path_object["journeys"].append({"routes": journeys})
                    else:
                        path_object["journeys"].append({"routes": [{"error": "No journeys found"}]})
                    node_index += 1
                else:
                    node_data = self.database_instance.getOneData("graph_nodes", {"index": path[node_index]})
                    path_object["journeys"].append({"node": node_data})
                    node_index += 1

            routes.append(path_object)
                
        return routes[0]