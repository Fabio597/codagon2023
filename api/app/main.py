from pydantic import BaseModel
from fastapi import FastAPI
from app.model.graph import GraphWrapper
from app.model.graph_builder import GraphBuilder

app = FastAPI()

graph_builder = GraphBuilder()
nodes, edges = graph_builder.create_nodes_and_edges()
graph_milan = GraphWrapper(nodes, edges)

class Route(BaseModel):
    starting_point: int
    ending_point: int
    interests: list[str]
    preferences: list[str]

@app.post("/routes/")
def update_item(item: Route):
    results = graph_milan.create_routes(item.starting_point, item.ending_point, item.interests, item.preferences, 10)
    return {"item_start": item.starting_point, "item_end": item.ending_point, "item_interests": item.interests, "results": results}