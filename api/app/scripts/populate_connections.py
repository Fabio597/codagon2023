import requests
from tqdm import tqdm

import os
from pymongo import MongoClient

MONGO_INITDB_ROOT_USERNAME = os.environ['MONGO_INITDB_ROOT_USERNAME']
MONGO_INITDB_ROOT_PASSWORD = os.environ['MONGO_INITDB_ROOT_PASSWORD']
MONGO_ENDPOINT= os.environ['MONGO_ENDPOINT']
MONGO_PORT = os.environ['MONGO_PORT']

class Database:

    def __init__(self) -> None:
        self.mongo_client = MongoClient(
            MONGO_ENDPOINT, 
            int(MONGO_PORT), 
            username=MONGO_INITDB_ROOT_USERNAME, 
            password=MONGO_INITDB_ROOT_PASSWORD, 
            authSource='admin'
        )
        
    def get_collection(self, collection_name):
        return self.mongo_client["codagon"][collection_name]
    
    def insertManyData(self, collection_name, data):
        collection = self.get_collection(collection_name)
        collection.insert_many(data)

    def insertOneData(self, collection_name, data):
        collection = self.get_collection(collection_name)
        collection.insert_one(data)

    def getData(self, collection_name, query):
        collection = self.get_collection(collection_name)
        return collection.find(query, {"_id": 0})
    
    def getOneData(self, collection_name, query):
        collection = self.get_collection(collection_name)
        return collection.find_one(query, {"_id": 0})
    
    def create_index(self, collection_name, column_name, unique=True):
        collection = self.get_collection(collection_name)
        collection.create_index([ (column_name, 1) ], unique=unique)

class Point:

    def __init__(self, lat: float, long: float):
        self.long = long
        self.lat = lat

    def __str__(self):
        return f'({self.lat}, {self.long})'
    
    def __repr__(self):
        return f'({self.lat}, {self.long})'
    
    def __eq__(self, other):
        return self.lat == other.lat and self.long == other.long
    
    def __hash__(self):
        return hash((self.lat, self.long))

database_instance = Database()

import requests

def get_navitia_response(starting_point: Point, ending_point: Point):

    http_url = f"https://api.navitia.io/v1/coverage/it/journeys?from={starting_point.long};{starting_point.lat}&to={ending_point.long};{ending_point.lat}&datetime=20231116T160000&first_section_mode%5B%5D=walking&first_section_mode%5B%5D=bike&first_section_mode%5B%5D=car_no_park&"

    headers = {
        'Origin': http_url,
        'Authorization': '2ca388fe-351a-4a22-806e-0d54b7fc4a7d'
    }
    return requests.get(http_url, headers=headers)

# get all nodes
poi = list(database_instance.getData("graph_nodes", {"node_type": {"$in": ["EVENT_PLACE", "POI"]}}))
bikes = list(database_instance.getData("graph_nodes", {"node_type": "BIKE_PARKING"}))
cars = list(database_instance.getData("graph_nodes", {"node_type": "AUTO_PARKING"}))
metro = list(database_instance.getData("graph_nodes", {"node_type": "METRO"}))
nodes = []
edges = []

# CREATE NODES and EDGES
# connect all point of interests
for node in tqdm(poi):
    for node2 in poi:
        if node['index'] != node2['index']:
            edges.append((node['index'], node2['index']))
    nodes.append({"index": node['index'], "node_type": node['node_type'], "name": node['name']})

# parcheggi bici -> punti di interesse
for bike_park in tqdm(bikes):
    for node in poi:
        edges.append((bike_park['index'], node['index']))
    nodes.append({"index": bike_park['index'], "node_type": bike_park['node_type'], "name": node['name']})

# parcheggi auto -> punti di interesse
for car_park in tqdm(cars):
    for node in poi:
        edges.append((car_park['index'], node['index']))
    nodes.append({"index": car_park['index'], "node_type": car_park['node_type'], "name": car_park['name']})

# fermate metro -> punti di interesse
for metro_stop in tqdm(metro):
    for node in poi:
        edges.append((metro_stop['index'], node['index']))
    nodes.append({"index": metro_stop['index'], "node_type": metro_stop['node_type'], "name": metro_stop['name']})

cache = []
for edge in tqdm(edges):
    start = database_instance.getOneData("graph_nodes", {"index": edge[0]})
    end = database_instance.getOneData("graph_nodes", {"index": edge[1]})
    response = get_navitia_response(Point(start['lat'], start['long']), Point(end['lat'], end['long']))
    if response.status_code == 200:
        cache.append({"start_node": edge[0], "end_node": edge[1], "navitia_response": response.json()})
    
    if len(cache) > 100:
        database_instance.insertManyData("navitia_routes_v2", cache)
        cache = []

if len(cache) > 0:
    database_instance.insertManyData("navitia_routes_v2", cache)
    cache = []